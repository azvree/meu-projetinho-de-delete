let url = "https://in-api-treinamento.herokuapp.com/posts/";
let $ = document;

let pai = $.querySelector("#listaId");

fetch(url)
.then( resp => resp.json())
.then( resp => {
    resp.forEach((itemArray) => {

        let name = itemArray.name;
        let mens = itemArray.message;
        let filho = $.createElement("li");
        filho.innerText = `${name}  diz : ${mens}\n`;
        pai.appendChild(filho);
    })
})


const myHeaders = {
    "content-type": "application/json"
}

pai.addEventListener('click', (e) => {

    let id = e.target.value; // nao consigo pegar o valor =(
    pai.removeChild(e.target);

    console.log(id);

    let fetchConfig = {
        method:"DELETE",
        headers: myHeaders,
    }

    fetch(url+id,fetchConfig).then(console.log)
});